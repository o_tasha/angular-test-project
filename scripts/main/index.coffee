dcApp = angular.module "dcApp", []

dcApp.config ($routeProvider) ->
  $routeProvider.when '/',
    controller: 'MainCtrl'

  $routeProvider.when '/list',
    controller: 'ListCtrl'



class Task
  constructor: ->
    @items = []

  addItem:(item)->
    @items.push item

  remove:(index) ->
    @items.splice index, 1

  getItems: ->
    @items



dcApp.factory "Task", ->
  new Task

dcApp.controller "MainCtrl", ($scope, Task)->
  $scope.task = Task
  $scope.newItem = {}

  $scope.selectRow = (event) ->
    elem = event.srcElement;
    if elem.className == 'selected'
      elem.className = ''
    else
      elem.className = 'selected'

  $scope.addItem = (item) ->
    @task.addItem item
    @newItem = {}

  $scope.remove = (index) ->
    @task.remove index