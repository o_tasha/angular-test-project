var Task, dcApp;

dcApp = angular.module("dcApp", []).
    config(function($routeProvider) {
        $routeProvider.
            when('/edit/:index', {controller: EditCtrl, templateUrl: 'scripts/main/views/edit.html'}).
            when('/', {controller: MainCtrl, templateUrl: 'scripts/main/views/list.html'}).
            otherwise({redirectTo:'/'});
    });

Task = (function() {
    function Task() {
        this.items = [];
    }

    Task.prototype.addItem = function(item) {
        return this.items.push(item);
    };

    Task.prototype.editItem = function(index, item) {
        this.items[index].name = item.name;
        this.items[index].type = item.type;
        return this.items;
    };

    Task.prototype.remove = function(index) {
        return this.items.splice(index, 1);
    };

    Task.prototype.getItems = function() {
        return this.items;
    };

    Task.prototype.getItem = function(index) {
        return this.items[index];
    };

    return Task;
})();

dcApp.factory("Task", function() {
    return new Task;
});

function MainCtrl($scope, Task) {
    $scope.task = Task;
    $scope.newItem = {};
    $scope.selectRow = function(event) {
        var elem;
        elem = event.target;
        if (elem.className === 'selected') {
            return elem.className = '';
        } else {
            return elem.className = 'selected';
        }
    };
    $scope.addItem = function(item) {
        this.task.addItem(item);
        return this.newItem = {};
    };
    return $scope.remove = function(index) {
        return this.task.remove(index);
    };
}

function EditCtrl($scope, $location, $routeParams, Task) {
    $scope.item = Task.getItem($routeParams.index);
    $scope.index = $routeParams.index;
    $scope.saveItem = function(index, item) {
        Task.editItem(index, item);
        $location.path('/');
    };
}


